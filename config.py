import pprint
import os

def config(pard):
	import site_config
	
	for k in [
			'HTTP_PROTOCOL',
			'HTTP_ADDRESS',
			'HTTP_PORT',
			'db.mysql',
			'SMTP_HOST',
			'SMTP_PORT',
			'SMTP_USER',
			'SMTP_PASSW',
			'DEFAULT_SENDER',
			'TEMPLATE_RELOAD',
			'with_static',
			'with_ws',
			'ws_auth',
			'ws_version',
			'cookie_secret',
			'DEBUG',
			'LOGLEVEL',
			'GOOGLE',
			'EMAIL',
			]:
		pard[k] = site_config.config.get(k)
	
	pard['APPLICATION_NAME'] = 'Numeri'
	
	_HTTP_PORT = ':' + str(pard['HTTP_PORT']) if pard['HTTP_PORT'] <> 80 else ''
	pard['APPSERVER'] = '%(HTTP_PROTOCOL)s://%(HTTP_ADDRESS)s' % pard + _HTTP_PORT
	
	pard['PROJECT'] = 'numeri'
	pard['PROJECT_SUBTITLE'] = 'Distributore di numeri'
	pard['PROJECT_TITLE'] = 'Numeri'
	pard['TITLE'] = 'Prendi un Numero'

	pard['LANGUAGES'] = ['it', 'en']

	pard['AUTH_MODULE'] = 'wsgian.gauth'
	pard['LOGIN_MODULE'] = 'wsgian.gauth'
	pard['LOGIN_URL'] = '/login'
	pard['LOGOUT_URL'] = '/logout'
	pard['SIGNUP_URL'] = '/signup'
	
	return pard


def build_default_config():
	if os.path.isfile('site_config.py'):
		pass
	else:
		print 'Creating default site configuration. . .',
		d = {			
			# --------------------------------------------------- #
			# APPSERVER
			# --------------------------------------------------- #
			'HTTP_PROTOCOL': 'http',
			'HTTP_ADDRESS': 'localhost',
			'HTTP_PORT': 8080,
			
			# --------------------------------------------------- #
			# Email configuration
			# --------------------------------------------------- #
			'DEFAULT_SENDER': 'AppEmail@YourDomain.com',
			
			# --------------------------------------------------- #
			# Data Base
			# --------------------------------------------------- #
			'db.mysql': {
				'host': 'mysqlhost',
				'user': 'mysqluser',
				'password': 'mysqlpassword',
				'database': 'mysqldb',
				'max_idle_time': 7*3600},

			# --------------------------------------------------- #
			# Development Server
			# --------------------------------------------------- #			
			'cookie_secret' : 'notapplicable',
			'DEBUG': True,
			'LOGLEVEL': 'debug',
			'with_static': True,
			'TEMPLATE_RELOAD': True,
			
			}
		buf = 'config = ' + pprint.pformat(d)
		open('site_config.py', 'w').write(buf)
		print 'done!'


def create_database():
	pard = config({})
	import wsgian.mysql
	pard['mysql.conn'] = wsgian.mysql.Connection(**pard['db.mysql'])
	pard['mysql.conn'].close()
	

if __name__ == '__main__':
	import sys
	action = sys.argv[1] if len(sys.argv) > 1 else ''
	if action == 'db':
		create_database()
	elif action == 'test':
		pard = config({})
		pprint.pprint(pard)
	else:
		build_default_config()
