'''
Web App Dispenser di numeri.

Appunti
-------
*** Da fare in Gestione Coda ***
- Stampare QR Code per assegnare i numeri
- Resettare la coda
- Cancellare la coda (deleted)
- elenco persone da servire
- condivisione del link per prendere un numero dalla coda


'''
from wsgian import gauth
import wsgian.utils
import bulma
import coda


def main(pard):
	pard.setdefault('action', '')

	if pard['action'] in ('', 'start'):
		code_utente = coda.code_utente(pard, {'id_user': pard['sid_user']})
		pard['main_body'] = render_start(pard, code_utente)
		pard['html'] = render_page(pard)

	elif pard['action'] == 'crea':
		pard['main_body'] = render_edit(pard, {})
		pard['html'] = render_page(pard)

	elif pard['action'] == 'edit':
		rec = coda.get_coda(pard, {'id_coda': pard['id_coda']})
		pard['main_body'] = render_edit(pard, rec)
		pard['html'] = render_page(pard)

	elif pard['action'] == 'nuova_coda':
		rec = wsgian.utils.cgi_params(pard, 'rec')
		try:
			id_coda = coda.crea(pard, rec)
			pard['redirect'] = '/app/coda/gest/%s' % id_coda
		except coda.Error as err:
			pard['errori'] = [str(err)]
			pard['main_body'] = render_edit(pard, rec)
			pard['html'] = render_page(pard)

	elif pard['action'] == 'gest_coda':
		rec = coda.get_coda(pard, {'id_coda': pard['id_coda']})
		rec['stats'] = coda.stats(pard, rec)
		rec['in_corso'] = coda.in_corso(pard, rec)
		pard['main_body'] = render_gest_coda(pard, rec)
		pard['html'] = render_page(pard)

	elif pard['action'] == 'next':
		args = {}
		args['id_coda'] = pard['id_coda']
		args['numero'] = coda.prossimo_numero(pard, args)
		if args['numero']:
			coda.numero_in_corso(pard, args)
		pard['redirect'] = '/app/coda/gest/%(id_coda)s' % args

	elif pard['action'] in ('done', 'done_and_next'):
		args = {}
		args['id_coda'] = pard['id_coda']
		args['numero'] = pard['numero']
		coda.numero_servito(pard, args)
		if pard['action'] == 'done_and_next':
			args['numero'] = coda.prossimo_numero(pard, args)
			if args['numero']:
				coda.numero_in_corso(pard, args)
		pard['redirect'] = '/app/coda/gest/%(id_coda)s' % args	

	elif pard['action'] == 'undo':
		args = {}
		args['id_coda'] = pard['id_coda']
		args['numero'] = pard['numero']
		coda.numero_assegnato(pard, args)
		pard['redirect'] = '/app/coda/gest/%(id_coda)s' % args	

	elif pard['action'] == 'help':
		pard['main_body'] = render_help_gestione_coda(pard)
		pard['html'] = render_page(pard)

	else:
		pard['html'] = wsgian.utils.dump(pard['action'])
	
	return pard


def render_edit(pard, rec):
	rec.setdefault('id_coda', '')
	rec.setdefault('descrizione', '')
	rec.setdefault('tipo', 'classica')
	rec.setdefault('qta', 999)
	if not rec['id_coda']:
		rec['id_coda'] = coda.proponi_id(pard, {'len': 4})
	
	if pard['action'] in ('crea', 'nuova_coda'):
		rec['input_id_coda'] = bulma.input(pard,
				{'id': 'input_id_coda',
				 'name': 'rec[id_coda]',
				 'label': 'Scegli un codice per il tuo nuovo dispenser di numeri',
				 'value': rec['id_coda'],
				 'help_message': 'Scegli un codice oppure conferma quello proposto',
				 'help_class': 'is-info',
				 'icon_left': 'fa-tape',
				})
	else:
		rec['input_id_coda'] = bulma.input(pard,
				{'id': 'input_id_coda',
				 'name': 'rec[id_coda]',
				 'label': 'Codice Dispenser',
				 'value': rec['id_coda'],
				 'help_class': 'is-info',
				 'icon_left': 'fa-tape',
				 'disabled': 'disabled',
				})
	
	rec['select_tipo'] = bulma.select(pard,
			{'id': 'select_tipo',
			 'name': 'rec[tipo]',
			 'label': 'Tipo Dispenser',
			 'value': rec['tipo'],
			 'help_message': 'Classico: eroga un numero progressivo ad ogni richiesta.<br>Casuale: eroga un numero a caso che ancora non &egrave; stato scelto.',
			 'help_class': 'is-info',
			 'selected': 'classica',
			 'options': [('classica', 'Classico'), ('casuale', 'Casuale')],
			})
	rec['input_qta'] = bulma.input(pard,
			{'id': 'input_qta',
			 'name': 'rec[qta]',
			 'label': 'Numeri nel dispenser',
			 'value': rec['qta'],
			 'help_message': 'Scegli quanti numeri mettere nel dispenser',
			 'help_class': 'is-info',
			})
	rec['textarea_descrizione'] = bulma.textarea(pard,
			{'id': 'textarea_descrizione',
			 'name': 'rec[descrizione]',
			 'label': 'Descrizione',
			 'value': rec['descrizione'],
			 'placeholder': 'Se vuoi, puoi descrivere qui il tuo dispenser. Ad esempio: &laquo;Questo &egrave; il dispenser elimina-code ecologico del mio negozio!&raquo;',
			})
	
	if pard['action'] in ('crea', 'nuova_coda'):
		rec['action'] = 'nuova_coda'
		rec['href_annulla'] = '/app'
	else:
		rec['action'] = 'modifica_coda'
		rec['href_annulla'] = '/app/coda/gest/%(id_coda)s' % rec
	
	h = '''
		<form enctype="multipart/form-data" id="numeriForm" method="post" action="/app/coda">		
		<input type="hidden" name="action" id="action" value="%(action)s">

		%(input_id_coda)s
		%(select_tipo)s
		%(input_qta)s
		%(textarea_descrizione)s
		
		<div class="field is-grouped">
  		  <div class="control">
    	    <a class="button is-link is-light" href="%(href_annulla)s">Annulla</a>
	      </div>
	  	  <div class="control">
    	    <button class="button is-link" onclick="document.getElementById('numeriForm').submit()">Salva</button>
  		  </div>
	    </div>
	    </form>
		''' % rec
	return h


def render_gest_coda(pard, rec):
	card = {}
	card['header'] = 'Dispenser %(id_coda)s' % rec
	card['header_icon'] = {
		'aria_label': 'Modifica',
		'href': '/app/coda/edit/%(id_coda)s' % rec,
		'icon': 'fa-edit',
		}
	
	if rec['stats']['assegnati'] == 1:
		rec['persone_in_coda'] = "C'&egrave; una persona in coda."
	elif rec['stats']['assegnati'] > 1:
		rec['persone_in_coda'] = "Ci sono %(assegnati)s persone in coda." % rec['stats']
	else:
		rec['persone_in_coda'] = "Non ci sono persone in coda."
		
	if rec['stats']['serviti'] == 1:
		rec['persone_servite'] = "Hai servito una persona."
	elif rec['stats']['serviti'] > 1:
		rec['persone_servite'] = "Hai servito %(serviti)s persone." % rec['stats'] 
	else:
		rec['persone_servite'] = "Non hai ancora servito nessuno."
		
	rec['stai_servendo'] = render_persone_in_coda(pard, rec)
	rec['tag_apri_servizio'] = ''
	if rec['stats']['assegnati'] > 0:
		rec['tag_apri_servizio'] = '''
			<a class="tag is-dark" href="/app/coda/next/%(id_coda)s">
				Inizia a servire un'altra persona
			</a>
			''' % rec
		
	rec['APPSERVER'] = pard['APPSERVER']
	rec['input_link'] = '''
		<label class="label has-text-weight-normal">Condividi</label>
		<div id="input_link" class="field has-addons">
		  <div class="control is-expanded">
		    <input class="input is-small" type="text" placeholder="" value="%(APPSERVER)s/app/coda/condividi/%(id_coda)s" readonly>
		  </div>
		  <p class="control">
			<button id="copy_button" class="button is-black is-small">Copy</button>
		  </p>
		</div>
		<p class="help">Copia questo link e invialo via email o messaggio ai tuoi utenti/clienti/amici per permettergli di prendere un numero da questo dispenser.</p>
		<p class="help"><a class="tag is-dark" href="/app/coda/qrcode/%(id_coda)s">Stampa QR Code</a> del Dispenser.</p>
		''' % rec
	## Aggiungo il javascript per copiare il link
	pard.setdefault('javascript', '')
	pard['javascript'] += '''
		<script>
		(function() {
			const copy_link = function () {
				let the_link = document.querySelector('#input_link input');
				the_link.select(); 
  				the_link.setSelectionRange(0, 99999); /* For mobile devices */

			    /* Copy the text inside the text field */
				document.execCommand("copy");
			    
			    /* Alert the copied text */
			    alert("Link copiato. Ora puoi incollarlo in un messaggio o in una mail e inviarlo");
			};
			let copy_button = document.getElementById('copy_button');
			copy_button.addEventListener("click", copy_link);
		})();
		</script>
		'''
	
	card['content'] = '''	
		<div class="content is-small">
			<p>
			  %(descrizione)s</br>
			  <b><i>%(id_coda)s</i></b> &egrave; una coda %(tipo)s di %(qta)s numeri.</br>
			  %(persone_servite)s %(persone_in_coda)s
			</p>
		</div>
		<div class="block">
			%(stai_servendo)s
			%(tag_apri_servizio)s
		</div>
		<div class="block">%(input_link)s</div>
		''' % rec
	
	card['footer'] = [
		{'content': 'Indietro', 'href': '/app'},
		{'content': 'Help', 'href': '/app/coda/help/%(id_coda)s' % rec},
		]
	h = bulma.card(pard, card)
	return h


def render_persone_in_coda(pard, rec):
	in_corso = rec['in_corso']
	if not in_corso:
		return ''
	
	if len(in_corso) > 1:
		ae = 'persone'
	else:
		ae = 'persona'
	
	h = []
	h.append('''
		<div class="field">
		  <label class="label">Stai servendo {num} {persone}:</label>
		</div>
		'''.format(num=len(in_corso), persone=ae))
	for num in in_corso:
		h.append('''
			<div class="field has-addons">
				<p class="control is-expanded">
					<input class="input is-small" type="text" value="%(numero)s - %(upd_user)s" readonly>
				</p>
				<p class="control">
					<a class="button is-info is-small" href="/app?&id_coda=%(id_coda)s&numero=%(numero)s&action=done">
						Servito
					</a>
				</p>
				<p class="control">
					<a class="button is-success is-small" href="/app?&id_coda=%(id_coda)s&numero=%(numero)s&action=done_and_next">
						Prossimo
					</a>
				</p>
				<p class="control">
					<a class="button is-success is-small is-danger" href="/app?&id_coda=%(id_coda)s&numero=%(numero)s&action=undo">
						<span class="icon">
      						<i class="fas fa-minus-circle"></i>
						</span>
					</a>
				</p>
			</div>
			''' % num)
	
	return '\n'.join(h)


def render_help_gestione_coda(pard):
	card = {}
	card['header'] = 'Gestione Dispenser'
	card['content'] = '''
		<div class="block">
			<p class="help"><b>Inizia a servire un'altra persona:</b> Per iniziare a servire una persona in pi&ugrave; rispetto a quelle che stai gi&agrave; servendo.</p>
			<p class="help"><b>QR Code:</b> Stampa un foglio con un QR Code identificativo del Dispenser. Le persone potranno scansionare il codice per <i>prendere</i> un numero.</p>
			<p class="help">
				<span class="icon-text">
				  <span class="icon">
					<i class="fas fa-edit"></i>
				  </span>
				  <span>
				    Per Modificare i dati del Dispenser: cambiare la descrizione, aggiungere numeri, resettare o cancellare.
				  </span>
				</span>
			</p>
			<p class="help"><b>Copy:</b> Copia il link per condividere il dispenser con altre persone e permettergli di prendere numeri.</p>
		</div>
		<div class="block">
			<p class="help"><b>Servito:</b> Per completare il servizio della persona.</p>
			<p class="help"><b>Prossimo:</b> Per completare il servizio della persona e iniziare a servire la successiva in coda.</p>
			<p class="help">
				<span class="icon-text">
				  <span class="icon">
					<i class="fas fa-minus-circle"></i>
				  </span>
				  <span>
				    Per rimettere in coda il numero selezionato.
				  </span>
				</span>
			</p>
		</div>
		'''
	card['footer'] = [
		{'content': 'Indietro', 'href': '/app/coda/gest/%(id_coda)s' % pard},
		]
	h = bulma.card(pard, card)
	return h


# ------------------------------------------------------------------- #
def render_page(pard):
	pard.setdefault('javascript', '')
	pard.setdefault('errori', [])
	pard['notification'] = ''
	pard['navbar'] = render_navbar(pard)
	if pard['errori']:
		pard['notification'] = bulma.notification(pard, 
				{'class': 'is-danger',
				 'is-light': 'is-light',
				 'messaggi': pard['errori'],
				})
		pard['javascript'] += bulma.notification_js(pard)
	html = '''
	<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>%(TITLE)s</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
		<script defer src="https://use.fontawesome.com/releases/v5.14.0/js/all.js"></script>
	  </head>
	  <body>
	  %(navbar)s
	  <section class="section">
		<div class="container">
		
		<div class="columns">
		<div class="column is-full-mobile is-10-desktop is-offset-1-desktop">
		%(notification)s
		%(main_body)s
		</div> <!-- column -->
		</div> <!-- columns -->
		
		</div>
	  </section>
	  </body>
	  %(javascript)s
	</html>
	''' % pard	
	return html


def render_navbar(pard):
	pard['user_menu'] = render_user_menu(pard)
	h = '''
	<nav class="navbar has-shadow" role="navigation" aria-label="main navigation">
	<div class="navbar-brand">
  
	<a class="navbar-item" href="%(APPSERVER)s">
	  <span class="icon is-medium has-text-danger">
	    <i class="fas fa-2x fa-tape"></i>
	  </span>
	  <span class="title is-4 has-text-danger">Numeri</span>
	</a>
	
	<a class="navbar-item" href="/app/crea" >
	  <span class="icon">
	    <abbr title="Crea un nuovo dispenser di numeri">
	    <i class="fas fa-2x fa-plus-square"></i>
	    </abbr>
	  </span>
	</a>

	<a class="navbar-item" href="/app/cerca" >
	  <span class="icon">
	    <abbr title="Cerca un dispenser di numeri">
	    <i class="fas fa-2x fa-search"></i>
	    </abbr>
	  </span>
	</a>

		
    <a id="navbar_burger" role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarNumeri" onclick="toggleBurger()">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>

	</div>


	<div id="navbar_numeri" class="navbar-menu">
	  <div class="navbar-start">
	  </div>
	    
	  <div class="navbar-end">
	  %(user_menu)s
	  </div>		
	</div>
	
	</nav>
	
	<script>
	const toggleBurger = () => {
		let burgerIcon = document.getElementById('navbar_burger');
		let dropMenu = document.getElementById('navbar_numeri');
		burgerIcon.classList.toggle('is-active');
		dropMenu.classList.toggle('is-active');
	  };
	const toggleUser = () => {
		let user_menu = document.getElementById('user_menu_dropdown');
		user_menu.classList.toggle('is-active');
	  };
	</script>
	
	''' % pard
	return h


def render_user_menu(pard):
	if pard['user_data']['fullname']:
		pard['user_html'] = pard['user_data']['fullname']
	else:
		pard['user_html'] = pard['user_data']['email']
	
	pard['account_settings_url'] = gauth.get_account_setting_url()
	user_menu = '''
		<div id="user_menu_dropdown" class="navbar-item has-dropdown">
		  <a class="navbar-link" onclick="toggleUser()">
			%(user_html)s
		  </a>
		  <div class="navbar-dropdown is-right">
		    <a class="navbar-item" href="%(account_settings_url)s">
		      Settings
			</a>
			<hr class="navbar-divider">
			<a class="navbar-item" href="%(LOGOUT_URL)s">
			  Logout
			</a>
		  </div>
		 </div>
		''' % pard
	return user_menu


def render_start(pard, code_utente):
	paneld = {}
	paneld['heading'] = 'Dispenser Attivi'
	paneld['panel_blocks'] = []
	for rec in code_utente:
		if not rec['descrizione']:
			rec['descrizione'] = rec['id_coda']
				
		content = rec['descrizione']
		for num in rec['in_corso']:
			content += '&nbsp;<span class="tag is-rounded is-danger">%(numero)s</span>' % num
					
		paneld['panel_blocks'].append(
			bulma.panel_block(pard,
				{'icon': 'fa-qrcode',
				 'content': content,
				 'href': '/app/coda/gest/%(id_coda)s' % rec}))
	paneld['panel_blocks'].append('''
		<div class="panel-block">
		  <a class="button is-link is-outlined is-fullwidth" href="/app/crea">
			Crea un Dispenser di Numeri
		  </a>
		</div>
		''')
	h = bulma.panel(pard, paneld)
	#h += wsgian.utils.dump(code_utente)
	return h

