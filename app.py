import config
from wsgian import gauth
import sys
import wsgian

urls = [		
	{'pattern': '/',  'module': 'home'},	
	{'pattern': '/app', 'module': 'web', 'login': True},
	{'pattern': '/app/coda', 'module': 'web', 'login': True},
	{'pattern': '/app/coda/gest/{id_coda}', 'module': 'web', 'action': 'gest_coda', 'login': True},
	{'pattern': '/app/coda/next/{id_coda}', 'module': 'web', 'action': 'next', 'login': True},
	{'pattern': '/app/coda/help/{id_coda}', 'module': 'web', 'action': 'help', 'login': True},
	{'pattern': '/app/coda/edit/{id_coda}', 'module': 'web', 'action': 'edit', 'login': True},
	{'pattern': '/app/crea', 'module': 'web', 'action': 'crea', 'login': True},
	{'pattern': '/app/cerca', 'module': 'web', 'action': 'cerca', 'login': True},
	## provvisorio. il cerca sara' aperto ?
]

try:
	config = config.config({})
except ImportError:
	print 'config not found: run "python config.py" then edit site_config.py'
	sys.exit(1)

gauth.test_database(config)

urls.extend(gauth.auth_urls(config))

app = wsgian.App(urls, config)

if __name__ == '__main__':
	wsgian.quickstart(app, config['HTTP_ADDRESS'], config['HTTP_PORT'])

