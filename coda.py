
#
# API di gestione delle code
#

import logging
import random
import wsgian
from wsgian import gauth

__all_rest__ = [
	'crea',
	'get_by_id',
	'assegna_numero',
	'numero_servito',
	'numero_in_corso',
	'prossimo_numero',
	'reset',
	'proponi_id',
	]

# ----------------------------------------------------------------- #
# Errori
# ----------------------------------------------------------------- #
class Error(Exception):
	"""Base class for other exceptions"""
	pass

class NotFound(Error):
	"""Raised when the record is not found"""
	pass

class WrongVersion(Error):
	"""Raised when the user try to update a old version of the record"""
	pass

class PermissionDenied(Error):
	"""Raised when the user haven't the permission to update the record"""
	pass

class InvalidUser(Error):
	"""Raised when user is not found"""
	pass

class InvalidData(Error):
	"""Raised when input args is not valid"""
	pass

class Locked(Error):
	"""Raised when record is locked"""

class Empty(Error):
	"""Raised when record is empty"""


# ----------------------------------------------------------------- #
# create table
# ----------------------------------------------------------------- #

def drop_table(pard, table_name):
	sql = 'drop table if exists %s' % table_name
	pard['mysql.conn'].execute(sql)
	

def create_table(pard):
	pard.setdefault('drop_table', False)
	if pard['drop_table']: drop_table(pard, 'code')
	sql = """
		create table code (
			id_coda         varchar(32)  NOT NULL default '',
			descrizione     varchar(255) not null default '',
			stato           varchar(20)  not null default '' comment '[,deleted]',
			blocca          varchar(32)  not null default '',
			tipo            varchar(20)  not null default 'calssica' comment '[calssica,casuale]',
			qta             smallint unsigned not null default 999,  
			
			proprietario    varchar(50)  NOT NULL DEFAULT '',
			upd_user        varchar(50)  NOT NULL DEFAULT '',
		 	upd_ts          timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			
			primary key (id_coda)
			)
		"""
	pard['mysql.conn'].execute(sql)

	if pard['drop_table']: drop_table(pard, 'numeri')
	sql = """
		create table numeri (
			id_coda         varchar(32)  NOT NULL default '',
			numero          int unsigned not null default 0,
			stato           varchar(20)  not null default '' comment '[,assegnato,servito]',
			
			upd_user        varchar(50)  NOT NULL DEFAULT '',
		 	upd_ts          timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			
			primary key (id_coda, numero)
			)
		"""
	pard['mysql.conn'].execute(sql)


# ----------------------------------------------------------------- #
# methods
# ----------------------------------------------------------------- #

def crea(pard, args):
	'''
	Crea una coda
	
	Args:
		id_coda, facoltativo: e' possibile creare una coda con un nome
		                      scelto dall'utente
		id_user,
		descrizione, facoltativo
		tipo, [*classica, casuale]
		qta
	
	Returns:
		id_coda
	'''
	args.setdefault('descrizione', '')
	args.setdefault('id_user', pard.get('sid_user', ''))
	args.setdefault('id_coda', '')
	args.setdefault('tipo', 'classica')
	args.setdefault('qta', 999)
	
	user = gauth.get_user(pard, args['id_user'])
	if not user:
		raise InvalidUser('Utente `%s` non trovato' 
				% wsgian.utils.cgi_escape(args['id_user']))
	
	if args['id_coda']:
		try:
			args['id_coda'] = wsgian.validate.ascii_string(
					args['id_coda'], 'Codice coda', lang='it')
		except Exception, err:
			raise InvalidData(err.message)
		rec = get_by_id(pard, {'id_coda': args['id_coda'], 'deleted': True})
		if rec:
			raise InvalidData('Codice gia` utilizzato, prova con un altro')
	else:
		args['id_coda'] = proponi_id(pard, {'len': 4})
	
	if args['tipo'] not in ('classica', 'casuale'):
		raise InvalidData('Tipo coda non previsto')
	
	try:
		args['qta'] = wsgian.validate.to_int(args['qta'], 'Quantita`', lang='it')
	except ValueError, err:
		raise InvalidData(err.message)
	
	if args['qta'] > 100000:
		raise InvalidData('Stai chiedendo troppi numeri (si puo` sempre ricominciare da capo)')
		
	
	sql = """
		INSERT INTO code
			(id_coda, descrizione, proprietario, tipo, qta, upd_user)
		VALUES
			(%(id_coda)s, %(descrizione)s, %(id_user)s, %(tipo)s, %(qta)s, %(id_user)s)
		"""
	pard['mysql.conn'].execute(sql, args)
	
	rec_num = [{'id_coda': args['id_coda'], 'numero': n} for n in xrange(1,args['qta']+1)]
	
	sql = """
		insert into numeri
			(id_coda, numero)
		values
			(%(id_coda)s, %(numero)s)
		"""
	pard['mysql.conn'].executemany(sql, rec_num)
	
	return args['id_coda']


def proponi_id(pard, args):
	args.setdefault('len', 4)
	try:
		args['len'] = wsgian.validate.to_int(
				args['len'],
				'Numero caretteri codice coda',
				lang='it')
	except ValueError, err:
		raise InvalidData(err.message)
	while True:
		args['id_coda'] = wsgian.utils.get_alpha_token()[0:args['len']]
		rec = get_by_id(pard, args)
		if not rec:
			break
		else:
			logging.info('trovato un duplicato %(id_coda)s, riprovo' % args)
	return args['id_coda']


def get_by_id(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('deleted', False)
	if len(args['id_coda']) < 4:
		raise InvalidData('Codice coda non valido')
	sql = """
		select
			*
		from
			code
		where
			id_coda = %(id_coda)s
		"""
	if not args['deleted']:
		sql += " and stato <> 'deleted'"
	rec = pard['mysql.conn'].get(sql, args)
	return rec


def get_coda(pard, args):
	"""
	Restituisce la coda, controllando i permessi
	di gestione e modifica.
	"""
	args.setdefault('id_coda', '')
	args.setdefault('id_user', pard.get('sid_user', ''))
	args.setdefault('deleted', False)
	user = gauth.get_user(pard, args['id_user'])
	if not user:
		raise InvalidUser('Utente `%s` non valido' 
				% wsgian.utils.cgi_escape(args['id_user']))
	coda = get_by_id(pard, args)
	if not coda:
		raise NotFound('Coda non trovata')
	if args['id_user'] <> coda['proprietario']:
		raise PermissionDenied('Utente non autorizzato')
	return coda


def check_permission(pard, args):
	coda = get_coda(pard, args)
	return coda


def blocca(pard, args):
	args.setdefault('id_coda', '')
	rec = get_by_id(pard, args)
	if not rec:
		raise NotFound('Coda non trovata')
	if rec['blocca']:
		raise Locked('Coda bloccata riprova')
	args['token'] = wsgian.utils.get_token()
	sql = """
		update
			code
		set
			blocca = %(token)s
		where
			id_coda = %(id_coda)s
			and blocca = ''
		"""
	pard['mysql.conn'].execute(sql, args)
	rec = get_by_id(pard, args)
	if rec['blocca'] != args['token']:
		raise Locked('Coda bloccata riprova')
	return rec


def sblocca(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('blocca', '')
	sql = """
		update
			code
		set
			blocca = ''
		where
			id_coda = %(id_coda)s
			and blocca = %(blocca)s
		"""
	pard['mysql.conn'].execute(sql, args)
	return 'Done'


def assegna_numero(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('id_user', pard.get('sid_user', ''))
	args.setdefault('sid', '')
	if not args['sid']:
		raise InvalidData('Numero sessione (sid) obbligatorio per questa operazione')
	
	## Se c'e' un numero assegnato allo stesso sid in stato assegnato
	## viene restituito il numero stesso
	sql = """
		select
			numero
		from
			numeri
		where
			id_coda = %(id_coda)s
			and upd_user = %(sid)s
			and stato = 'assegnato'
		"""
	rec_num = pard['mysql.conn'].get(sql, args)
	if rec_num:
		return rec_num['numero']

	## Blocco la coda
	coda = blocca(pard, args)
	
	## Legge i numeri da assegnare
	sql = """
		select
			numero
		from
			numeri
		where
			id_coda = %(id_coda)s
			and stato = ''
		order by
			numero
		"""
	result = pard['mysql.conn'].query(sql, args)

	if not result:
		sblocca(pard, coda)
		raise Empty('Non ci sono rimasti numeri da estrarre')
	elif coda['tipo'] == 'classica':
		numero = result[0]['numero']
	else:
		ll = [rec['numero'] for rec in result]
		numero = random.choice(ll)
	
	args['numero'] = numero

	## Aggiorna il numero come 'assegnato'
	sql = """
		update
			numeri
		set
			stato = 'assegnato',
			upd_user = %(sid)s
		where
			id_coda = %(id_coda)s
			and numero = %(numero)s
		"""
	pard['mysql.conn'].execute(sql, args)
	
	sblocca(pard, coda)
	
	return numero


def numero_in_corso(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('numero', '')
	coda = check_permission(pard, args)
	sql = """
		update
			numeri
		set
			stato = 'in_corso'
		where
			id_coda = %(id_coda)s
			and numero = %(numero)s
		"""
	pard['mysql.conn'].execute(sql, args)


def numero_assegnato(pard, args):
	"""undo da in_corso a assegnato"""
	args.setdefault('id_coda', '')
	args.setdefault('numero', '')
	coda = check_permission(pard, args)
	sql = """
		update
			numeri
		set
			stato = 'assegnato'
		where
			id_coda = %(id_coda)s
			and numero = %(numero)s
		"""
	pard['mysql.conn'].execute(sql, args)


def numero_servito(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('numero', '')
	coda = check_permission(pard, args)
	sql = """
		update
			numeri
		set
			stato = 'servito'
		where
			id_coda = %(id_coda)s
			and numero = %(numero)s
		"""
	pard['mysql.conn'].execute(sql, args)


def prossimo_numero(pard, args):
	args.setdefault('id_coda', '')
	sql = """
		select
			min(numero) as numero
		from
			numeri 
		where
			id_coda = %(id_coda)s
			and stato = 'assegnato'
		"""
	num = pard['mysql.conn'].query(sql, args)
	if num and num[0]['numero']:
		return num[0]['numero']
	else:
		return 0


def reset(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('id_user', pard.get('sid_user', ''))
	coda = check_permission(pard, args)

	sql = """
		update
			numeri
		set
			stato = '',
			upd_user = ''
		where
			id_coda = %(id_coda)s
		"""
	pard['mysql.conn'].execute(sql, args)


def stats(pard, args):
	args.setdefault('id_coda', '')
	args.setdefault('id_user', pard.get('sid_user', ''))
	sql = """
		select
			sum(if(stato='assegnato',1,0)) as assegnati,
			sum(if(stato='',1,0)) as `da assegnare`,
			sum(if(stato='in_corso',1,0)) as `in_corso`,
			sum(if(stato='servito',1,0)) as `serviti`,
			count(*) as totale
		from
			numeri
		where
			id_coda = %(id_coda)s
		"""
	res = pard['mysql.conn'].query(sql, args)
	if not res:
		return {}
	else:
		return res[0]


def in_corso(pard, args):
	args.setdefault('id_coda', '')
	sql = """
		select
			id_coda, numero, upd_user
		from
			numeri 
		where
			id_coda = %(id_coda)s
			and stato = 'in_corso'
		"""
	num = pard['mysql.conn'].query(sql, args)
	return num


def code_utente(pard, args):
	args.setdefault('id_user', pard.get('sid_user', ''))
	user = gauth.get_user(pard, args['id_user'])
	if not user:
		raise InvalidUser('Utente `%s` non valido' 
				% wsgian.utils.cgi_escape(args['id_user']))
	sql = """
		select
			*
		from
			code
		where
			proprietario = %(id_user)s
			and stato <> 'deleted'
		order by
			upd_ts desc
		"""
	result = pard['mysql.conn'].query(sql, args)
	for rec in result:
		rec['stats'] = stats(pard, rec)
		rec['in_corso'] = in_corso(pard, rec)
	return result


# ----------------------------------------------------------------- #
if __name__ == '__main__':
	import config
	import wsgian.mysql
	import pprint
	pard = config.config({})
	pard['mysql.conn'] = wsgian.mysql.Connection(**pard['db.mysql'])
	pard['drop_table'] = True
	create_table(pard)


	