
from wsgian import template
import wsgian.utils


def main(pard):
	pard.setdefault('action', '')
	if pard['action'] in ('', 'start'):
		if pard['user_data']:
			pard['redirect'] = '/app'
		else:
			return render_home(pard)		
	else:
		#pard['html'] = wsgian.utils.dump(pard['action'])
		pard['html'] = wsgian.utils.dump(pard['user_data'])
	
	return pard


def render_home(pard):
	html = '''
	<!DOCTYPE html>
	<html>
	  <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>%(TITLE)s</title>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.1/css/bulma.min.css">
	  </head>
	  <body>
	  <section class="section">
		<div class="container has-text-centered">
		  <h1 class="title is-1">%(PROJECT_TITLE)s</h1>
		  <h3 class="subtitle is-4">%(PROJECT_SUBTITLE)s</h3>
		  <br>
		  <p>
		     <a class="button is-primary" href="%(SIGNUP_URL)s">Sign-up it's free</a>
		  </p>
		  <br>
		  <br> 
		  <p>
		     Already have an account?
		     <a class="is-link" href="%(LOGIN_URL)s">Log in</a>
		  </p>
		</div>
	  </section>
	  </body>
	</html>
	''' % pard
	
	pard['html'] = html
	return pard

